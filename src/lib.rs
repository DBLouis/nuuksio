//! A single-thread runtime for asynchronous applications.
#![cfg(target_os = "linux")]
// Rust lints
#![warn(future_incompatible)]
#![warn(nonstandard_style)]
#![warn(rust_2018_idioms)]
#![deny(let_underscore_drop)]
#![deny(missing_debug_implementations)]
//#![warn(missing_docs)]
#![deny(unsafe_op_in_unsafe_fn)]
// Clippy lints
#![warn(clippy::all)]
#![warn(clippy::correctness)]
#![warn(clippy::suspicious)]
#![warn(clippy::complexity)]
#![warn(clippy::perf)]
#![warn(clippy::style)]
#![warn(clippy::cargo)]
//#![deny(clippy::missing_safety_doc)]
//#![deny(clippy::missing_panics_doc)]
//#![deny(clippy::missing_errors_doc)]
//#![deny(clippy::undocumented_unsafe_blocks)]

pub mod executor;
pub mod future;
pub mod net;
pub mod reactor;
pub mod runtime;
pub mod sync;
pub mod task;
pub mod time;

pub use crate::runtime::Runtime;
