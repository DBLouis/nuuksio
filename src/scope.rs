use crate::{task::{Handle}, runtime::Runtime};

use tracing::trace;

use std::{cell::{Cell}, future::Future, marker::PhantomData};

/// A scope to spawn tasks into.
///
/// See [std::thread::Scope](https://doc.rust-lang.org/std/thread/struct.Scope.html)
#[derive(Debug)]
#[repr(transparent)]
pub struct Scope<'s, 'e: 's> {
    rt: Runtime,
    count: Cell<usize>,
    /// Invariance over 's, to make sure 's cannot shrink,
    /// which is necessary for soundness.
    ///
    /// Without invariance, this would compile fine but be unsound:
    ///
    /// ```compile_fail
    /// use nuuksio::Runtime;
    /// use std::io;
    ///
    /// let rt = Runtime::new()?;
    ///
    /// rt.scope(|s| {
    ///     s.spawn(async move {
    ///         let a = String::from("abcd");
    ///         s.spawn(async {
    ///             println!("{a:?}") // might run after `a` is dropped
    ///         });
    ///     });
    /// });
    ///
    /// # Ok::<(), io::Error>(())
    /// ```
    scope: PhantomData<Cell<&'s ()>>,
    env: PhantomData<Cell<&'e ()>>,
}

impl<'s, 'e> Scope<'s, 'e> {
    pub fn new() -> Self {
        Self { rt: Runtime::get(), count: Cell::new(0), scope: PhantomData, env: PhantomData }
    }

    pub fn count(&self) -> usize {
        self.count.get()
    }

    pub fn spawn<F>(&'s self, future: F) -> Handle<'s, F>
    where
        F: Future + 's,
        F::Output: 's,
    {
        self.count.set(self.count.get() + 1);
        let guard = ScopeGuard::new(&self.count);
        unsafe { self.rt.spawn_unchecked(guard, future) }
    }
}

#[derive(Debug)]
#[repr(transparent)]
pub struct ScopeGuard<'s> {
    count: Option<&'s Cell<usize>>,
}

impl<'s> ScopeGuard<'s> {
    pub fn new(count: &'s Cell<usize>) -> Self {
        Self { count: Some(count) }
    }
}

impl ScopeGuard<'static> {
    pub fn new_static() -> Self {
        Self { count: None }
    }
}

impl Drop for ScopeGuard<'_> {
    fn drop(&mut self) {
        trace!("scope guard drop");
        if let Some(count) = self.count {
            count.set(count.get() - 1);
        }
    }
}
