use either::Either;
use pin_project::pin_project;
use std::{
    future::Future,
    pin::{pin, Pin},
    task::{Context, Poll},
};

use crate::runtime::Runtime;

pub async fn yield_now() {
    #[repr(transparent)]
    struct YieldNow {
        rt: Runtime,
        yielded: bool,
    }

    impl Future for YieldNow {
        type Output = ();

        fn poll(mut self: Pin<&mut Self>, _: &mut Context<'_>) -> Poll<()> {
            if self.yielded {
                return Poll::Ready(());
            }

            self.yielded = true;
            let tid = self.rt.tid();
            self.rt.with_executor(|e| e.wake(tid));
            Poll::Pending
        }
    }

    YieldNow { rt: Runtime::get(), yielded: false }.await;
}

// biased toward `left`
pub async fn race<L, R>(left: L, right: R) -> Either<L::Output, R::Output>
where
    L: Future,
    R: Future,
{
    #[pin_project]
    struct Race<L, R> {
        #[pin]
        left: L,
        #[pin]
        right: R,
    }

    impl<L, R> Future for Race<L, R>
    where
        L: Future,
        R: Future,
    {
        type Output = Either<L::Output, R::Output>;

        fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
            let this = self.project();
            match this.left.poll(cx) {
                Poll::Pending => this.right.poll(cx).map(Either::Right),
                ready @ Poll::Ready(_) => ready.map(Either::Left),
            }
        }
    }

    Race { left, right }.await
}

pub async fn zip<A, B>(a: A, b: B) -> (A::Output, B::Output)
where
    A: Future,
    B: Future,
{
    let mut a = pin!(a);
    let mut b = pin!(b);
    match race(&mut a, &mut b).await {
        Either::Left(a) => (a, b.await),
        Either::Right(b) => (a.await, b),
    }
}

pub async fn try_zip<A, B, E, T, U>(a: A, b: B) -> Result<(T, U), E>
where
    A: Future<Output = Result<T, E>>,
    B: Future<Output = Result<U, E>>,
{
    let mut a = pin!(a);
    let mut b = pin!(b);
    match race(&mut a, &mut b).await {
        Either::Left(a) => b.await.and_then(|b| a.map(|a| (a, b))),
        Either::Right(b) => a.await.and_then(|a| b.map(|b| (a, b))),
    }
}
