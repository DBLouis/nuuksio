use crate::{reactor::Interest, runtime::Runtime};

use rustix::{
    fd::{AsRawFd, OwnedFd},
    io,
    io::Errno,
    net,
    net::{AddressFamily, RecvFlags, SendFlags, SocketAddrAny, SocketFlags, SocketType},
};

#[cfg(feature = "ipv4")]
pub use rustix::net::{Ipv4Addr as IpAddr, SocketAddrV4 as SocketAddr};
#[cfg(feature = "ipv6")]
pub use rustix::net::{Ipv6Addr as IpAddr, SocketAddrV6 as SocketAddr};

use tracing::{trace, trace_span, Instrument};

use std::{
    future::poll_fn,
    task::{Context, Poll},
};

#[derive(Debug)]
#[repr(transparent)]
pub struct TcpStream {
    fd: OwnedFd,
    rt: Runtime,
}

impl TryFrom<OwnedFd> for TcpStream {
    type Error = Errno;

    fn try_from(fd: OwnedFd) -> Result<Self, Self::Error> {
        let rt = Runtime::get();
        rt.with_reactor(|r| r.add(&fd))?;
        Ok(Self { fd, rt })
    }
}

impl TcpStream {
    fn new() -> io::Result<Self> {
        let family = if cfg!(feature = "ipv4") {
            AddressFamily::INET
        } else if cfg!(feature = "ipv6") {
            AddressFamily::INET6
        } else {
            unimplemented!()
        };
        let flags = SocketFlags::CLOEXEC | SocketFlags::NONBLOCK;
        let fd = net::socket_with(family, SocketType::STREAM, flags, None)?;
        //net::sockopt::set_socket_reuseaddr(&fd, true)?;
        Self::try_from(fd)
    }

    fn poll_connect(&mut self, _: &mut Context<'_>, addr: &SocketAddr) -> Poll<io::Result<()>> {
        #[cfg(feature = "ipv4")]
        let result = net::connect_v4(&self.fd, addr);
        #[cfg(feature = "ipv6")]
        let result = net::connect_v6(&self.fd, addr);
        match result {
            Err(Errno::INPROGRESS) => {
                let tid = self.rt.tid();
                self.rt.with_reactor(|r| r.register(&self.fd, Interest::writable(), tid))?;
                Poll::Pending
            }
            Err(e) => Poll::Ready(Err(e)),
            Ok(()) => Poll::Ready(Ok(())),
        }
    }

    pub async fn connect(addr: &SocketAddr) -> io::Result<Self> {
        let mut stream = Self::new()?;
        poll_fn(|cx| stream.poll_connect(cx, addr)).await?;
        Ok(stream)
    }
}

impl TcpStream {
    fn poll_recv(&mut self, _: &mut Context<'_>, buf: &mut [u8]) -> Poll<io::Result<usize>> {
        match net::recv(&self.fd, buf, RecvFlags::empty()) {
            Err(Errno::WOULDBLOCK) => {
                trace!("would block");
                let tid = self.rt.tid();
                if let Err(e) = self.rt.with_reactor(|r| r.register(&self.fd, Interest::readable(), tid)) {
                    Poll::Ready(Err(e))
                } else {
                    Poll::Pending
                }
            }
            Err(e) => Poll::Ready(Err(e)),
            Ok(0) => Poll::Ready(Err(Errno::CONNRESET)),
            Ok(n) => Poll::Ready(Ok(n)),
        }
    }

    fn poll_send(&mut self, _: &mut Context<'_>, buf: &[u8]) -> Poll<io::Result<usize>> {
        match net::send(&self.fd, buf, SendFlags::empty()) {
            Err(Errno::WOULDBLOCK) => {
                trace!("would block");
                let tid = self.rt.tid();
                if let Err(e) = self.rt.with_reactor(|r| r.register(&self.fd, Interest::writable(), tid)) {
                    Poll::Ready(Err(e))
                } else {
                    Poll::Pending
                }
            }
            Err(e) => Poll::Ready(Err(e)),
            Ok(n) => Poll::Ready(Ok(n)),
        }
    }

    pub async fn recv(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        let fd = self.fd.as_raw_fd();
        poll_fn(move |cx| self.poll_recv(cx, buf)).instrument(trace_span!("recv", "{}", fd)).await
    }

    pub async fn send(&mut self, buf: &[u8]) -> io::Result<usize> {
        let fd = self.fd.as_raw_fd();
        poll_fn(move |cx| self.poll_send(cx, buf)).instrument(trace_span!("send", "{}", fd)).await
    }

    pub fn shutdown(&mut self) -> io::Result<()> {
        net::shutdown(&self.fd, net::Shutdown::ReadWrite)?;
        Ok(())
    }
}

#[derive(Debug)]
#[repr(transparent)]
pub struct TcpListener {
    fd: OwnedFd,
    rt: Runtime,
}

impl TcpListener {
    fn new() -> io::Result<Self> {
        let family = if cfg!(feature = "ipv4") {
            AddressFamily::INET
        } else if cfg!(feature = "ipv6") {
            AddressFamily::INET6
        } else {
            unimplemented!()
        };
        let flags = SocketFlags::CLOEXEC | SocketFlags::NONBLOCK;
        let fd = net::socket_with(family, SocketType::STREAM, flags, None)?;
        let rt = Runtime::get();
        rt.with_reactor(|r| r.add(&fd))?;
        Ok(Self { fd, rt })
    }

    fn poll_bind(&mut self, _: &mut Context<'_>, addr: &SocketAddr) -> Poll<io::Result<()>> {
        #[cfg(feature = "ipv4")]
        let result = net::bind_v4(&self.fd, addr);
        #[cfg(feature = "ipv6")]
        let result = net::bind_v6(&self.fd, addr);
        match result {
            Err(Errno::INPROGRESS) => {
                let tid = self.rt.tid();
                if let Err(e) = self.rt.with_reactor(|r| r.register(&self.fd, Interest::writable(), tid)) {
                    Poll::Ready(Err(e))
                } else {
                    Poll::Pending
                }
            }
            Err(e) => Poll::Ready(Err(e)),
            Ok(()) => Poll::Ready(Ok(())),
        }
    }

    pub async fn bind(addr: &SocketAddr) -> io::Result<Self> {
        let mut listener = Self::new()?;
        poll_fn(|cx| listener.poll_bind(cx, addr)).await?;
        net::listen(&listener.fd, 16)?;
        Ok(listener)
    }

    fn poll_accept(&mut self, _: &mut Context<'_>) -> Poll<io::Result<(TcpStream, SocketAddr)>> {
        let flags = SocketFlags::CLOEXEC | SocketFlags::NONBLOCK;
        match net::acceptfrom_with(&self.fd, flags) {
            Err(Errno::WOULDBLOCK) => {
                let tid = self.rt.tid();
                if let Err(e) = self.rt.with_reactor(|r| r.register(&self.fd, Interest::readable(), tid)) {
                    Poll::Ready(Err(e))
                } else {
                    Poll::Pending
                }
            }
            Err(e) => Poll::Ready(Err(e)),
            #[cfg(feature = "ipv4")]
            Ok((fd, Some(SocketAddrAny::V4(addr)))) => {
                let stream = TcpStream::try_from(fd)?;
                Poll::Ready(Ok((stream, addr)))
            }
            #[cfg(feature = "ipv6")]
            Ok((fd, Some(SocketAddrAny::V6(addr)))) => {
                let stream = TcpStream::try_from(fd)?;
                Poll::Ready(Ok((stream, addr)))
            }
            Ok(_) => unimplemented!(),
        }
    }

    pub async fn accept(&mut self) -> io::Result<(TcpStream, SocketAddr)> {
        poll_fn(|cx| self.poll_accept(cx)).instrument(trace_span!("accept")).await
    }
}
