use crate::reactor::Interest;
use crate::runtime::Runtime;

use pin_project::pin_project;
use rustix::{
    fd::{AsFd, BorrowedFd, OwnedFd},
    io,
    io::Errno,
    time::{timerfd_create, timerfd_settime, Itimerspec, TimerfdClockId, TimerfdFlags, TimerfdTimerFlags, Timespec},
};

use std::{
    future::Future,
    mem,
    mem::MaybeUninit,
    pin::Pin,
    task::{Context, Poll},
    time::Duration,
};

/// # Panics
///
/// Panics if an error occurs while creating the timer.
pub fn sleep(duration: Duration) -> Timer {
    Timer::one_shot(duration).unwrap()
}

/// # Panics
///
/// Panics if an error occurs while creating the timer.
pub fn interval(period: Duration) -> Timer {
    Timer::interval(period).unwrap()
}

/// # Panics
///
/// Panics if an error occurs while creating the timer.
pub fn timeout<F>(duration: Duration, future: F) -> Timeout<F>
where
    F: Future,
{
    let timer = Timer::one_shot(duration).unwrap();
    Timeout { timer, future }
}

#[derive(Debug)]
#[pin_project]
pub struct Timeout<F> {
    timer: Timer,
    #[pin]
    future: F,
}

impl<F> Timeout<F> {
    pub fn into_inner(self) -> F {
        self.future
    }
}

impl<F> Future for Timeout<F>
where
    F: Future,
{
    type Output = io::Result<F::Output>;

    fn poll(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let mut this = self.as_mut().project();

        match Pin::new(&mut this.timer).poll(cx) {
            Poll::Ready(Ok(())) => Poll::Ready(Err(Errno::TIMEDOUT)),
            Poll::Ready(Err(e)) => Poll::Ready(Err(e)),
            Poll::Pending => this.future.poll(cx).map(Ok),
        }
    }
}

#[derive(Debug)]
#[repr(transparent)]
pub struct Timer {
    rt: Runtime,
    fd: OwnedFd,
}

impl Timer {
    pub fn interval(period: Duration) -> io::Result<Self> {
        let expiration = Itimerspec {
            it_interval: Timespec {
                tv_sec: i64::try_from(period.as_secs()).unwrap(),
                tv_nsec: i64::from(period.subsec_nanos()),
            },
            it_value: Timespec { tv_sec: 0, tv_nsec: 1 },
        };
        Self::new(expiration)
    }

    pub fn one_shot(duration: Duration) -> io::Result<Self> {
        let expiration = Itimerspec {
            it_interval: Timespec { tv_sec: 0, tv_nsec: 0 },
            it_value: Timespec {
                tv_sec: i64::try_from(duration.as_secs()).unwrap(),
                tv_nsec: i64::from(duration.subsec_nanos()),
            },
        };
        Self::new(expiration)
    }

    fn new(expiration: Itimerspec) -> io::Result<Self> {
        let rt = Runtime::get();

        let flags = TimerfdFlags::NONBLOCK | TimerfdFlags::CLOEXEC;
        let fd = timerfd_create(TimerfdClockId::Monotonic, flags)?;
        rt.with_reactor(|r| r.add(&fd))?;

        let flags = TimerfdTimerFlags::empty();
        timerfd_settime(&fd, flags, &expiration)?;

        Ok(Self { rt, fd })
    }

    pub fn wait(&mut self) -> io::Result<()> {
        let mut buf = [MaybeUninit::uninit(); mem::size_of::<u64>()];
        let (buf, _) = io::read_uninit(&self.fd, &mut buf)?;
        let count = u64::from_ne_bytes(buf.try_into().unwrap());
        debug_assert_ne!(count, 0);
        Ok(())
    }
}

impl Future for Timer {
    type Output = io::Result<()>;

    fn poll(mut self: Pin<&mut Self>, _: &mut Context<'_>) -> Poll<Self::Output> {
        match self.wait() {
            Err(Errno::WOULDBLOCK) => {
                let tid = self.rt.tid();
                if let Err(e) = self.rt.with_reactor(|r| r.register(&self.fd, Interest::readable(), tid)) {
                    Poll::Ready(Err(e))
                } else {
                    Poll::Pending
                }
            }
            result => Poll::Ready(result),
        }
    }
}

impl AsFd for Timer {
    fn as_fd(&self) -> BorrowedFd<'_> {
        self.fd.as_fd()
    }
}
