mod id;

pub use id::TaskId;

use std::{
    cell::RefCell,
    fmt,
    future::{poll_fn, Future},
    pin::{pin, Pin},
    ptr,
    rc::Rc,
    task::{ready, Context, Poll, RawWaker, RawWakerVTable, Waker},
};

use pin_project::pin_project;

use crate::runtime::Runtime;

// TODO use qcell instead of RefCell
// TODO custom Rc with no weak references?

pub fn dummy_waker() -> Waker {
    const VTABLE: RawWakerVTable = RawWakerVTable::new(|_| RAW, |_| {}, |_| {}, |_| {});
    const RAW: RawWaker = RawWaker::new(ptr::null(), &VTABLE);
    unsafe { Waker::from_raw(RAW) }
}

#[derive(Debug)]
#[pin_project]
pub struct Task<F>
where
    F: Future,
{
    rt: Runtime,
    // tid for join task
    tid: Option<TaskId>,
    output: Option<F::Output>,
    #[pin]
    future: F,
}

impl<F> Task<F>
where
    F: Future,
{
    pub fn new(rt: Runtime, future: F) -> Self {
        Self { rt, tid: None, output: None, future }
    }

    fn join(self: Pin<&mut Self>, _: &mut Context<'_>) -> Poll<F::Output> {
        let this = self.project();
        if let Some(output) = this.output.take() {
            Poll::Ready(output)
        } else {
            *this.tid = Some(this.rt.tid());
            Poll::Pending
        }
    }
}

impl<F> Future for Task<F>
where
    F: Future,
{
    type Output = ();

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<()> {
        let this = self.project();
        let output = ready!(this.future.poll(cx));
        *this.output = Some(output);
        if let Some(tid) = this.tid.take() {
            this.rt.with_executor(|e| e.wake(tid));
        }
        Poll::Ready(())
    }
}

// TODO rename
pub struct RawHandle {
    task: Rc<RefCell<dyn Future<Output = ()>>>,
}

impl RawHandle {
    pub fn tid(&self) -> TaskId {
        TaskId::from_ptr(self.task.as_ptr() as *const ())
    }

    pub fn poll(&self, cx: &mut Context<'_>) -> Poll<()> {
        let task = &mut *self.task.borrow_mut();
        // TODO avoid unsafe here
        let task = unsafe { Pin::new_unchecked(task) };
        task.poll(cx)
    }
}

impl fmt::Debug for RawHandle {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("RawHandle").finish_non_exhaustive()
    }
}

impl fmt::Display for RawHandle {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "RawHandle({})", self.tid())
    }
}

pub struct Handle<F>
where
    F: Future,
{
    task: Rc<RefCell<Task<F>>>,
}

impl<F> From<Task<F>> for Handle<F>
where
    F: Future,
{
    fn from(task: Task<F>) -> Self {
        Self { task: Rc::new(RefCell::new(task)) }
    }
}

impl<F> Handle<F>
where
    F: Future + 'static,
{
    pub fn to_raw(&self) -> RawHandle {
        let task = Rc::clone(&self.task);
        RawHandle { task }
    }

    pub fn tid(&self) -> TaskId {
        TaskId::from_ptr(self.task.as_ptr() as *const ())
    }

    pub async fn join(self) -> F::Output {
        poll_fn(|cx| {
            let task = &mut *self.task.borrow_mut();
            // TODO avoid unsafe here
            let task = unsafe { Pin::new_unchecked(task) };
            task.join(cx)
        })
        .await
    }

    pub fn take_output(self) -> Option<F::Output> {
        let mut task = self.task.borrow_mut();
        task.output.take()
    }
}

impl<F> fmt::Debug for Handle<F>
where
    F: Future,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Handle").finish_non_exhaustive()
    }
}
