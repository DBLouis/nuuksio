//! Task synchronization

pub mod mutex;
pub mod rwlock;
pub mod semaphore;
pub mod watch;

pub use mutex::Mutex;
pub use rwlock::RwLock;
pub use semaphore::Semaphore;
pub use watch::Watcher;

use std::rc::Rc;

pub type MutexGuard<'a, T> = mutex::MutexGuard<&'a Mutex<T>, T>;
pub type OwnedMutexGuard<T> = mutex::MutexGuard<Rc<Mutex<T>>, T>;

pub type RwLockReadGuard<'a, T> = rwlock::RwLockReadGuard<&'a RwLock<T>, T>;
pub type RwLockWriteGuard<'a, T> = rwlock::RwLockWriteGuard<&'a RwLock<T>, T>;

pub type SemaphoreGuard<'a> = semaphore::SemaphoreGuard<&'a Semaphore>;
pub type OwnedSemaphoreGuard = semaphore::SemaphoreGuard<Rc<Semaphore>>;
