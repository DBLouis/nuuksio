use std::collections::{BTreeMap, VecDeque};
use std::fmt;

use static_assertions::assert_not_impl_all;
use tracing::{trace, warn};

use crate::task::{RawHandle, TaskId};

#[derive(Debug)]
pub struct Executor {
    // Idle tasks
    tasks: BTreeMap<TaskId, RawHandle>,
    // Awake tasks
    awake: VecDeque<TaskId>,
}

assert_not_impl_all!(Executor: Send, Sync);

impl Default for Executor {
    fn default() -> Self {
        Self::new()
    }
}

// TODO methods name are bad
impl Executor {
    pub const fn new() -> Self {
        Self { tasks: BTreeMap::new(), awake: VecDeque::new() }
    }

    pub fn wake(&mut self, tid: TaskId) {
        trace!("wake {}", tid);
        self.awake.push_back(tid);
    }

    /// # Panics
    ///
    /// Panics if the task is already in the executor.
    pub fn add(&mut self, handle: RawHandle) {
        let tid = handle.tid();
        trace!("add {}", tid);
        if self.tasks.insert(tid, handle).is_some() {
            panic!("duplicate task {tid}");
        }
        // Schedule task to be polled once
        self.wake(tid);
    }

    pub fn pop(&mut self) -> Option<RawHandle> {
        let tid = self.awake.pop_front()?;
        trace!("pop {}", tid);

        if let Some(handle) = self.tasks.remove(&tid) {
            debug_assert_eq!(tid, handle.tid());
            Some(handle)
        } else {
            warn!("unknown task {}", tid);
            None
        }
    }

    pub fn push(&mut self, handle: RawHandle) {
        let tid = handle.tid();
        trace!("push {}", tid);
        self.tasks.insert(tid, handle);
    }
}

impl fmt::Display for Executor {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Executor {{ tasks: {{ ")?;
        for (tid, handle) in &self.tasks {
            write!(f, "{tid}: {handle}, ")?;
        }
        write!(f, "}}, awake: {{ ")?;
        for tid in &self.awake {
            write!(f, "{tid}, ")?;
        }
        write!(f, "}} }}")
    }
}
