use std::fmt;

use rustix::{
    event::epoll,
    event::epoll::{CreateFlags, EventData, EventFlags, EventVec},
    fd::{AsFd, AsRawFd, OwnedFd},
    io,
    io::Errno,
};
use static_assertions::assert_not_impl_all;
use tracing::{trace, warn};

use crate::task::TaskId;

pub struct Reactor {
    fd: OwnedFd,
    buffer: EventVec,
}

assert_not_impl_all!(Reactor: Send, Sync);

impl Reactor {
    pub fn new() -> io::Result<Self> {
        let fd = epoll::create(CreateFlags::CLOEXEC)?;
        let buffer = EventVec::with_capacity(128);
        Ok(Self { fd, buffer })
    }

    pub fn add(&mut self, fd: impl AsFd) -> io::Result<()> {
        let fd = fd.as_fd();
        let data = EventData::new_u64(0);
        let flags = EventFlags::ET | EventFlags::ONESHOT;
        trace!("epoll add {}", fd.as_raw_fd());
        epoll::add(&self.fd, fd, data, flags)
    }

    /// Register a file descriptor with the reactor.
    pub fn register(&mut self, fd: impl AsFd, interest: Interest, tid: TaskId) -> io::Result<()> {
        let fd = fd.as_fd();

        #[allow(clippy::cast_sign_loss)]
        let data = EventData::new_u64({
            let value: usize = tid.into();
            debug_assert!(value > 0);
            value as u64
        });
        let flags = interest.flags | EventFlags::ET | EventFlags::ONESHOT;

        let result = {
            trace!("epoll modify {} {:?}", fd.as_raw_fd(), flags);
            epoll::modify(&self.fd, fd, data, flags)
        };
        let fd = fd.as_raw_fd();
        assert_ne!(result, Err(Errno::INVAL), "{fd} is an invalid file descriptor");
        assert_ne!(result, Err(Errno::EXIST), "{fd} is already registered");
        assert_ne!(result, Err(Errno::NOENT), "{fd} is not registered");
        result
    }

    pub fn wait(&mut self) -> impl Iterator<Item = TaskId> + '_ {
        trace!("epoll wait");

        #[cfg(not(miri))]
        match epoll::wait(&self.fd, &mut self.buffer, 1000) {
            Ok(()) => {}
            Err(Errno::TIMEDOUT) => {
                warn!("epoll wait timeout");
            }
            Err(err) => {
                panic!("epoll wait error: {}", err);
            }
        }

        self.buffer.iter().map(|event| {
            let value = event.data.u64() as usize;
            TaskId::try_from(value).unwrap()
        })
    }
}

impl fmt::Debug for Reactor {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Reactor").field("fd", &self.fd).finish_non_exhaustive()
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
#[repr(transparent)]
pub struct Interest {
    flags: EventFlags,
}

impl Interest {
    pub const fn readable() -> Self {
        Self { flags: EventFlags::IN }
    }
    pub const fn writable() -> Self {
        Self { flags: EventFlags::OUT }
    }
}
