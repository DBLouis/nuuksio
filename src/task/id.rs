use std::{fmt, num::NonZeroUsize};

/// An opaque task identifier.
///
/// It refers to a task which might or might not still exist in memory.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[repr(transparent)]
pub struct TaskId {
    value: NonZeroUsize,
}

impl TaskId {
    /// Creates a new task identifier from a raw task pointer.
    pub fn from_ptr(task: *const ()) -> Self {
        debug_assert!(!task.is_null());
        // SAFETY: the pointer is non-null so the value is non-zero.
        let value = unsafe { NonZeroUsize::new_unchecked(task as usize) };
        Self { value }
    }
}

impl TryFrom<usize> for TaskId {
    type Error = ();

    fn try_from(value: usize) -> Result<Self, Self::Error> {
        let value = NonZeroUsize::new(value).ok_or(())?;
        Ok(Self { value })
    }
}

impl From<TaskId> for usize {
    fn from(id: TaskId) -> Self {
        id.value.get()
    }
}

impl fmt::Display for TaskId {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let id = self.value.get() & 0xFFFF;
        write!(f, "#{id:04X}")
    }
}
