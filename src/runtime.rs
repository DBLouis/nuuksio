use std::{
    cell::{Cell, OnceCell, RefCell},
    future::Future,
    marker::PhantomData,
    panic::{catch_unwind, resume_unwind, AssertUnwindSafe},
    task::{Context, Poll},
};

use rustix::io;
use static_assertions::assert_not_impl_all;
use tracing::{debug_span, trace};

use crate::{
    executor::Executor,
    reactor::Reactor,
    task,
    task::{Handle, RawHandle, Task, TaskId},
};

thread_local! {
    /// The ID of the currently running task.
    static TID: Cell<Option<TaskId>> = const { Cell::new(None) };
    /// The executor for the current thread.
    static EXECUTOR: RefCell<Executor> = const { RefCell::new(Executor::new()) };
    /// The reactor for the current thread.
    static REACTOR: OnceCell<RefCell<Reactor>> = const { OnceCell::new() };
}

/// A handle to the runtime.
#[derive(Debug, Clone, Copy)]
#[repr(transparent)]
pub struct Runtime {
    _initialized: PhantomData<Cell<()>>,
}

assert_not_impl_all!(Runtime: Send, Sync);

impl Runtime {
    fn init() -> io::Result<()> {
        REACTOR.with(|r| {
            let reactor = Reactor::new()?;
            r.set(RefCell::new(reactor)).expect("reactor already initialized");
            Ok(())
        })
    }

    fn is_init() -> bool {
        REACTOR.with(|r| r.get().is_some())
    }

    /// # Safety
    ///
    /// Caller must ensure that the runtime is initialized.
    const unsafe fn new_unchecked() -> Self {
        Self { _initialized: PhantomData }
    }

    pub fn new() -> io::Result<Self> {
        Self::init()?;
        // SAFETY: The runtime has just been initialized.
        let rt = unsafe { Self::new_unchecked() };
        Ok(rt)
    }

    /// # Panics
    ///
    /// Panics if the runtime is not initialized.
    pub fn get() -> Self {
        assert!(Self::is_init(), "runtime not initialized");
        // SAFETY: Verified above.
        unsafe { Self::new_unchecked() }
    }

    /// # Safety
    ///
    /// Caller must ensure that the runtime is initialized.
    pub unsafe fn get_unchecked() -> Self {
        debug_assert!(Self::is_init(), "runtime not initialized");
        // SAFETY: Ensured by caller.
        unsafe { Self::new_unchecked() }
    }

    pub fn tid(&self) -> TaskId {
        TID.with(|t| t.get().unwrap())
    }

    // TODO document that futures are only polled during `block_on`
    pub fn block_on<F>(&self, future: F) -> F::Output
    where
        F: Future + 'static,
        F::Output: 'static,
    {
        let handle = self.spawn(future);
        self.run(handle.tid());
        handle.take_output().unwrap()
    }

    // TODO document that the future won't be polled until `block_on` is called
    pub fn spawn<F>(&self, future: F) -> Handle<F>
    where
        F: Future + 'static,
        F::Output: 'static,
    {
        let task = Task::new(*self, future);
        let handle = Handle::from(task);
        let tid = handle.tid();
        trace!("spawn task {}", tid);
        EXECUTOR.with_borrow_mut(|e| {
            e.add(handle.to_raw());
        });
        handle
    }

    #[inline(always)]
    pub fn with_reactor<F, R>(&self, f: F) -> R
    where
        F: FnOnce(&mut Reactor) -> R,
    {
        REACTOR.with(|reactor| {
            // SAFETY: The existence of `self` ensures that the runtime is initialized.
            let reactor = unsafe { reactor.get().unwrap_unchecked() };
            f(&mut reactor.borrow_mut())
        })
    }

    #[inline(always)]
    pub fn with_executor<F, R>(&self, f: F) -> R
    where
        F: FnOnce(&mut Executor) -> R,
    {
        EXECUTOR.with(|executor| f(&mut executor.borrow_mut()))
    }

    fn run(&self, root: TaskId) {
        loop {
            while let Some(handle) = EXECUTOR.with_borrow_mut(|e| e.pop()) {
                let tid = handle.tid();
                if self.poll(handle) && tid == root {
                    return;
                }
            }
            self.with_reactor(|r| {
                for tid in r.wait() {
                    trace!("task {} is ready", tid);
                    EXECUTOR.with_borrow_mut(|e| e.wake(tid));
                }
            });
            trace!("idle");
        }
    }

    fn poll(&self, handle: RawHandle) -> bool {
        let tid = handle.tid();
        trace!("task {} is awake", tid);

        // TODO have a way to assert waker is our dummy waker
        let waker = task::dummy_waker();
        let mut cx = Context::from_waker(&waker);

        TID.with(|t| t.set(Some(tid)));
        let span = debug_span!("task", %tid).entered();

        let result = catch_unwind(AssertUnwindSafe(|| handle.poll(&mut cx)));

        span.exit();
        TID.with(|t| t.set(None));

        match result {
            Ok(Poll::Pending) => {
                trace!("task {} is pending", tid);
                EXECUTOR.with_borrow_mut(|e| e.push(handle));
                false
            }
            Ok(Poll::Ready(())) => {
                drop(handle);
                trace!("task {} is ready", tid);
                true
            }
            Err(e) => {
                trace!("task {} panicked", tid);
                resume_unwind(e);
            }
        }
    }
}
