use super::semaphore::Semaphore;
use crate::future;

use either::Either;

use std::future::Future;

#[derive(Debug)]
pub struct Watcher {
    semaphore: Semaphore,
}

impl Default for Watcher {
    fn default() -> Self {
        Self::new()
    }
}

impl Watcher {
    pub fn new() -> Self {
        Self { semaphore: Semaphore::new(0) }
    }

    pub async fn watch<F>(&self, future: F) -> Option<F::Output>
    where
        F: Future,
    {
        match future::race(self.semaphore.acquire(usize::MAX), future).await {
            Either::Left(_) => {
                // `acquire` returned first, so the future was cancelled.
                None
            }
            Either::Right(output) => {
                // The future returned first, return its output.
                Some(output)
            }
        }
    }

    pub fn cancel(&self) {
        self.semaphore.restore(usize::MAX);
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::runtime::Runtime;

    #[test]
    fn watch_output() {
        let rt = Runtime::new().unwrap();
        let w = Watcher::new();
        let output = rt.block_on(async move { w.watch(async { 42 }).await });
        assert_eq!(output, Some(42));
    }

    #[test]
    fn watch_cancel() {
        let rt = Runtime::new().unwrap();
        let w = Watcher::new();
        let output = rt.block_on(async move {
            w.cancel();
            w.watch(async { 42 }).await
        });
        assert_eq!(output, None);
    }
}
