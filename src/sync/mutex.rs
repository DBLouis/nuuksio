use super::semaphore::Semaphore;

use std::{
    borrow::Borrow,
    cell::UnsafeCell,
    fmt,
    marker::PhantomData,
    ops::{Deref, DerefMut},
    rc::Rc,
};

use tracing::trace;

pub struct Mutex<T: ?Sized> {
    semaphore: Semaphore,
    value: UnsafeCell<T>,
}

impl<T> Mutex<T> {
    pub fn new(value: T) -> Self {
        Self { semaphore: Semaphore::new(1), value: UnsafeCell::new(value) }
    }

    pub fn into_inner(self) -> T {
        self.value.into_inner()
    }
}

impl<T: ?Sized> Mutex<T> {
    pub async fn lock(&self) -> MutexGuard<&'_ Self, T> {
        self.semaphore.acquire(1).await.forget();
        trace!("mutex locked");
        // SAFETY: A permit was acquired above.
        unsafe { MutexGuard::new(self) }
    }

    pub fn try_lock(&self) -> Option<MutexGuard<&'_ Self, T>> {
        if let Some(guard) = self.semaphore.try_acquire(1) {
            guard.forget();
            trace!("mutex locked");
            // SAFETY: A permit was acquired above.
            let guard = unsafe { MutexGuard::new(self) };
            Some(guard)
        } else {
            None
        }
    }

    pub async fn lock_owned(self: Rc<Self>) -> MutexGuard<Rc<Self>, T> {
        self.semaphore.acquire(1).await.forget();
        trace!("mutex locked");
        // SAFETY: A permit was acquired above.
        unsafe { MutexGuard::new(self) }
    }

    pub fn try_lock_owned(self: Rc<Self>) -> Option<MutexGuard<Rc<Self>, T>> {
        if let Some(guard) = self.semaphore.try_acquire(1) {
            guard.forget();
            trace!("mutex locked");
            // SAFETY: A permit was acquired above.
            let guard = unsafe { MutexGuard::new(self.clone()) };
            Some(guard)
        } else {
            None
        }
    }
}

impl<T: ?Sized> fmt::Debug for Mutex<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Mutex").field("semaphore", &self.semaphore).finish_non_exhaustive()
    }
}

#[must_use = "if unused the Mutex will immediately unlock"]
#[derive(Debug)]
pub struct MutexGuard<M, T: ?Sized>
where
    M: Borrow<Mutex<T>>,
{
    mutex: M,
    _value: PhantomData<T>,
}

impl<M, T: ?Sized> MutexGuard<M, T>
where
    M: Borrow<Mutex<T>>,
{
    // SAFETY: The mutex must be locked for the lifetime of the guard.
    const unsafe fn new(mutex: M) -> Self {
        Self { mutex, _value: PhantomData }
    }

    pub fn with(self, f: impl FnOnce(&T)) {
        f(&*self);
    }

    pub fn with_mut(mut self, f: impl FnOnce(&mut T)) {
        f(&mut *self);
    }
}

impl<M, T: ?Sized> Deref for MutexGuard<M, T>
where
    M: Borrow<Mutex<T>>,
{
    type Target = T;

    fn deref(&self) -> &T {
        // SAFETY: The existence of this guard means that a lock was acquired by
        // the current task, so no other tasks have a lock.
        // Therefore it is safe to create a shared reference to the inner value.
        unsafe { &*self.mutex.borrow().value.get() }
    }
}

impl<M, T: ?Sized> DerefMut for MutexGuard<M, T>
where
    M: Borrow<Mutex<T>>,
{
    fn deref_mut(&mut self) -> &mut T {
        // SAFETY: The existence of this guard means that a lock was acquired by
        // the current task, so no other tasks have a lock.
        // Therefore it is safe to create a mutable reference to the inner value.
        unsafe { &mut *self.mutex.borrow().value.get() }
    }
}

impl<M, T: ?Sized> Drop for MutexGuard<M, T>
where
    M: Borrow<Mutex<T>>,
{
    fn drop(&mut self) {
        self.mutex.borrow().semaphore.restore(1);
        trace!("mutex unlocked");
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn try_lock() {
        let mutex = Mutex::new(());
        let g1 = mutex.try_lock();
        assert!(g1.is_some());
        let g2 = mutex.try_lock();
        assert!(g2.is_none());
    }

    #[test]
    fn try_lock_owned() {
        let mutex = Rc::new(Mutex::new(()));
        let clone = mutex.clone();
        let g1 = mutex.try_lock_owned();
        assert!(g1.is_some());
        let g2 = clone.try_lock_owned();
        assert!(g2.is_none());
    }
}
