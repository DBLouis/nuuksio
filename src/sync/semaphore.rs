use std::{
    borrow::Borrow,
    cell::{Cell, UnsafeCell},
    future::Future,
    marker::PhantomPinned,
    pin::Pin,
    rc::Rc,
    task::{Context, Poll},
};

use intrusive_collections::{intrusive_adapter, LinkedList, LinkedListLink, UnsafeRef};
use pin_project::{pin_project, pinned_drop};
use static_assertions::assert_not_impl_all;
use tracing::trace;

use crate::{runtime::Runtime, task::TaskId};

/// A counting semaphore to perform asynchronous permission acquisition.
#[derive(Debug)]
pub struct Semaphore {
    rt: Runtime,
    /// An intrusive linked list of waiters which are stored in the [Acquire] future.
    queue: UnsafeCell<LinkedList<WaiterAdapter>>,
    /// The number of permits available.
    permits: Cell<usize>,
}

assert_not_impl_all!(Semaphore: Send, Sync);

impl Semaphore {
    /// Creates a new semaphore with the specified number of permits.
    pub fn new(permits: usize) -> Self {
        let rt = Runtime::get();
        Self { rt, queue: UnsafeCell::new(LinkedList::new(WaiterAdapter::new())), permits: Cell::new(permits) }
    }

    /// Acquires the specified number of permits. If the semaphore does not have enough permits,
    /// the current task will be waiting until enough permits are available.
    pub async fn acquire(&self, amount: usize) -> SemaphoreGuard<&'_ Self> {
        Acquire::new(self, amount).await
    }

    /// Acquires the specified number of permits. If the semaphore does not have enough permits,
    /// the current task will be waiting until enough permits are available.
    pub async fn acquire_owned(self: Rc<Self>, amount: usize) -> SemaphoreGuard<Rc<Self>> {
        Acquire::new(self, amount).await
    }

    /// Tries to acquire the specified number of permits without waiting.
    pub fn try_acquire(&self, amount: usize) -> Option<SemaphoreGuard<&'_ Self>> {
        if self.try_take(amount) {
            Some(SemaphoreGuard::new(self, amount))
        } else {
            None
        }
    }

    /// Tries to acquire the specified number of permits without waiting.
    pub fn try_acquire_owned(self: Rc<Self>, amount: usize) -> Option<SemaphoreGuard<Rc<Self>>> {
        if self.try_take(amount) {
            Some(SemaphoreGuard::new(self, amount))
        } else {
            None
        }
    }

    /// Restores the specified number of permits to the semaphore.
    pub fn restore(&self, amount: usize) {
        self.release(amount);
    }
}

intrusive_adapter!(WaiterAdapter = UnsafeRef<Waiter>: Waiter { link: LinkedListLink });

struct Waiter {
    link: LinkedListLink,
    tid: TaskId,
}

impl From<TaskId> for Waiter {
    fn from(tid: TaskId) -> Self {
        Self { link: LinkedListLink::new(), tid }
    }
}

impl Semaphore {
    fn try_take(&self, amount: usize) -> bool {
        trace!("try_take: amount={}", amount);
        let permits = self.permits.get();
        let (remaining, overflow) = permits.overflowing_sub(amount);
        if !overflow {
            self.permits.set(remaining);
        }
        !overflow
    }

    fn release(&self, amount: usize) {
        trace!("release: amount={}", amount);
        let permits = self.permits.get();
        self.permits.set(permits.checked_add(amount).unwrap());

        // SAFETY: A mutable reference to the queue is only created in three places:
        // Here, in `Semaphore::wait` and in `Semaphore::remove`.
        // In all cases, that reference lives at most until the end of the function and is not
        // kept across async boundaries. Therefore, and because `Semaphore` is not `Send`,
        // there is no mutable aliasing.
        let queue = unsafe { &mut *self.queue.get() };

        // Wake up one task if there is one waiting.
        if let Some(waiter) = queue.pop_front() {
            trace!("release: waking up tid={}", waiter.tid);
            self.rt.with_executor(|e| e.wake(waiter.tid));
        }
    }

    fn wait(&self, tid: TaskId, waiter: &mut Option<Waiter>) {
        trace!("wait: tid={}", tid);
        let old = waiter.replace(Waiter::from(tid));
        debug_assert!(old.is_none());
        // SAFETY: Option is `Some` because of the above replace.
        let waiter = unsafe { (*waiter).as_ref().unwrap_unchecked() };

        // SAFETY: This `UnsafeRef` will point to the `Waiter` created above as
        // long as the current task is waiting. Eventually, another task will
        // pop the `Waiter` from the queue and wake the current task. At that
        // point, the `UnsafeRef` will be dropped.
        // Because `Acquire` is not `Unpin`, the pointer to the `Waiter` will
        // remain valid for the lifetime of the future.
        let waiter = unsafe { UnsafeRef::from_raw(waiter as *const Waiter) };

        // SAFETY: See `Semaphore::release`.
        let queue = unsafe { &mut *self.queue.get() };
        queue.push_back(waiter);
    }

    fn remove(&self, waiter: &Waiter) {
        trace!("remove: tid={}", waiter.tid);
        // SAFETY: See `Semaphore::release`.
        let queue = unsafe { &mut *self.queue.get() };
        // SAFETY: TODO how do we know that the waiter is in the queue?
        let removed = unsafe { queue.cursor_mut_from_ptr(waiter).remove() };
        debug_assert!(removed.is_some());
    }
}

#[pin_project(PinnedDrop)]
struct Acquire<S>
where
    S: Borrow<Semaphore>,
{
    semaphore: S,
    #[pin]
    waiter: Option<Waiter>,
    amount: usize,
    // This type must be `!Unpin` because we are storing raw pointers to
    // `waiter` in the queue via `UnsafeRef`.
    pin: PhantomPinned,
}

impl<S> Acquire<S>
where
    S: Borrow<Semaphore>,
{
    const fn new(semaphore: S, amount: usize) -> Self {
        Self { semaphore, waiter: None, amount, pin: PhantomPinned }
    }
}

impl<S> Future for Acquire<S>
where
    S: Borrow<Semaphore> + Clone,
{
    type Output = SemaphoreGuard<S>;

    fn poll(self: Pin<&mut Self>, _: &mut Context<'_>) -> Poll<Self::Output> {
        let mut this = self.project();
        let semaphore: &Semaphore = (*this.semaphore).borrow();
        // First, try to take the permits, returning if successful.
        if semaphore.try_take(*this.amount) {
            let guard = SemaphoreGuard::new(this.semaphore.clone(), *this.amount);
            return Poll::Ready(guard);
        }
        // If there are not enough permits, make the current task wait.
        let tid = semaphore.rt.tid();
        semaphore.wait(tid, &mut this.waiter);
        Poll::Pending
    }
}

#[pinned_drop]
impl<S> PinnedDrop for Acquire<S>
where
    S: Borrow<Semaphore>,
{
    fn drop(self: Pin<&mut Self>) {
        // If the future is pending, remove the waiter from the queue.
        if let Some(waiter) = self.waiter.as_ref() {
            let semaphore = self.semaphore.borrow();
            semaphore.remove(waiter);
        }
    }
}

/// A guard that will release the acquired permits when dropped.
#[derive(Debug)]
pub struct SemaphoreGuard<S>
where
    S: Borrow<Semaphore>,
{
    semaphore: S,
    amount: usize,
}

impl<S> SemaphoreGuard<S>
where
    S: Borrow<Semaphore>,
{
    const fn new(semaphore: S, amount: usize) -> Self {
        Self { semaphore, amount }
    }

    /// Forgets the permits **without** releasing them back to the semaphore.
    pub fn forget(mut self) {
        self.amount = 0;
    }
}

impl<S> Drop for SemaphoreGuard<S>
where
    S: Borrow<Semaphore>,
{
    fn drop(&mut self) {
        self.semaphore.borrow().release(self.amount);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn try_acquire_zero() {
        let semaphore = Semaphore::new(0);
        assert!(semaphore.try_acquire(1).is_none());
    }

    #[test]
    fn try_acquire_one() {
        let semaphore = Semaphore::new(1);
        let guard = semaphore.try_acquire(1).unwrap();
        assert!(semaphore.try_acquire(1).is_none());
        drop(guard);
        assert!(semaphore.try_acquire(1).is_some());
    }

    #[test]
    fn forget() {
        let semaphore = Semaphore::new(1);
        let guard = semaphore.try_acquire(1).unwrap();
        guard.forget();
        assert!(semaphore.try_acquire(1).is_none());
    }

    #[test]
    fn restore() {
        let semaphore = Semaphore::new(1);
        let guard = semaphore.try_acquire(1).unwrap();
        guard.forget();
        assert!(semaphore.try_acquire(1).is_none());
        semaphore.restore(1);
        assert!(semaphore.try_acquire(1).is_some());
    }
}
