use super::semaphore::Semaphore;

use std::{
    borrow::Borrow,
    cell::UnsafeCell,
    fmt,
    marker::PhantomData,
    ops::{Deref, DerefMut},
};

pub struct RwLock<T: ?Sized> {
    semaphore: Semaphore,
    value: UnsafeCell<T>,
}

impl<T> RwLock<T> {
    pub fn new(value: T) -> Self {
        Self { semaphore: Semaphore::new(usize::MAX), value: UnsafeCell::new(value) }
    }

    pub fn into_inner(self) -> T {
        self.value.into_inner()
    }
}

impl<T: ?Sized> RwLock<T> {
    pub async fn read(&self) -> RwLockReadGuard<&'_ Self, T> {
        self.semaphore.acquire(1).await.forget();
        // SAFETY: Read lock was acquired above.
        unsafe { RwLockReadGuard::new(self) }
    }

    pub async fn write(&self) -> RwLockWriteGuard<&'_ Self, T> {
        self.semaphore.acquire(usize::MAX).await.forget();
        // SAFETY: Write lock was acquired above.
        unsafe { RwLockWriteGuard::new(self) }
    }

    pub fn try_read(&self) -> Option<RwLockReadGuard<&'_ Self, T>> {
        if let Some(guard) = self.semaphore.try_acquire(1) {
            guard.forget();
            // SAFETY: Read lock was acquired above.
            let guard = unsafe { RwLockReadGuard::new(self) };
            Some(guard)
        } else {
            None
        }
    }

    pub fn try_write(&self) -> Option<RwLockWriteGuard<&'_ Self, T>> {
        if let Some(guard) = self.semaphore.try_acquire(usize::MAX) {
            guard.forget();
            // SAFETY: Write lock was acquired above.
            let guard = unsafe { RwLockWriteGuard::new(self) };
            Some(guard)
        } else {
            None
        }
    }
}

impl<T: ?Sized> fmt::Debug for RwLock<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("RwLock").field("semaphore", &self.semaphore).finish_non_exhaustive()
    }
}

#[must_use]
#[derive(Debug)]
pub struct RwLockReadGuard<L, T: ?Sized>
where
    L: Borrow<RwLock<T>>,
{
    rwlock: L,
    _value: PhantomData<T>,
}

impl<L, T: ?Sized> RwLockReadGuard<L, T>
where
    L: Borrow<RwLock<T>>,
{
    // SAFETY: A read lock must be acquired before creating this guard.
    const unsafe fn new(rwlock: L) -> Self {
        Self { rwlock, _value: PhantomData }
    }

    pub fn with(self, f: impl FnOnce(&T)) {
        f(&*self);
    }
}

impl<L, T: ?Sized> Deref for RwLockReadGuard<L, T>
where
    L: Borrow<RwLock<T>>,
{
    type Target = T;

    fn deref(&self) -> &T {
        // SAFETY: The existence of this guard means that a read lock was acquired,
        // so no other task have a write lock (other tasks may have read locks).
        // Therefore, it is safe to create a shared reference to the inner value.
        unsafe { &*self.rwlock.borrow().value.get() }
    }
}

impl<L, T: ?Sized> Drop for RwLockReadGuard<L, T>
where
    L: Borrow<RwLock<T>>,
{
    fn drop(&mut self) {
        self.rwlock.borrow().semaphore.restore(1);
    }
}

#[must_use]
#[derive(Debug)]
pub struct RwLockWriteGuard<L, T: ?Sized>
where
    L: Borrow<RwLock<T>>,
{
    rwlock: L,
    _value: PhantomData<T>,
}

impl<L, T: ?Sized> RwLockWriteGuard<L, T>
where
    L: Borrow<RwLock<T>>,
{
    // SAFETY: A write lock must be acquired before creating this guard.
    const unsafe fn new(rwlock: L) -> Self {
        Self { rwlock, _value: PhantomData }
    }

    pub fn with(self, f: impl FnOnce(&T)) {
        f(&*self);
    }

    pub fn with_mut(mut self, f: impl FnOnce(&mut T)) {
        f(&mut *self);
    }
}

impl<L, T: ?Sized> Deref for RwLockWriteGuard<L, T>
where
    L: Borrow<RwLock<T>>,
{
    type Target = T;

    fn deref(&self) -> &T {
        // SAFETY: The existence of this guard means that a write lock was acquired,
        // so no other task have a write or read lock. Therefore, it is safe
        // to create a shared reference to the inner value.
        unsafe { &*self.rwlock.borrow().value.get() }
    }
}

impl<L, T: ?Sized> DerefMut for RwLockWriteGuard<L, T>
where
    L: Borrow<RwLock<T>>,
{
    fn deref_mut(&mut self) -> &mut T {
        // SAFETY: The existence of this guard means that a write lock was acquired,
        // so no other task have a write or read lock. Therefore, it is safe
        // to create a mutable reference to the inner value.
        unsafe { &mut *self.rwlock.borrow().value.get() }
    }
}

impl<L, T: ?Sized> Drop for RwLockWriteGuard<L, T>
where
    L: Borrow<RwLock<T>>,
{
    fn drop(&mut self) {
        self.rwlock.borrow().semaphore.restore(usize::MAX);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn try_read() {
        let lock = RwLock::new(());
        let g1 = lock.try_read();
        assert!(g1.is_some());
        let g2 = lock.try_read();
        assert!(g2.is_some());
    }

    #[test]
    fn try_read_then_write() {
        let lock = RwLock::new(());
        let g1 = lock.try_read();
        assert!(g1.is_some());
        let g2 = lock.try_write();
        assert!(g2.is_none());
    }

    #[test]
    fn try_write() {
        let lock = RwLock::new(());
        let g1 = lock.try_write();
        assert!(g1.is_some());
        let g2 = lock.try_write();
        assert!(g2.is_none());
    }

    #[test]
    fn try_write_then_read() {
        let lock = RwLock::new(());
        let g1 = lock.try_write();
        assert!(g1.is_some());
        let g2 = lock.try_read();
        assert!(g2.is_none());
    }
}
