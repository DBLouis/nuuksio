use nuuksio::{future::yield_now, runtime::Runtime, sync::RwLock};

use std::{cell::Cell, io, rc::Rc};

const LIMIT: u32 = 10;

#[test]
fn main() -> io::Result<()> {
    let rt = Runtime::new()?;

    let lock = RwLock::new(Cell::new(None));
    let lock = Rc::new(lock);

    rt.spawn(consumer(lock.clone()));
    rt.block_on(producer(lock.clone()));
    Ok(())
}

async fn producer(lock: Rc<RwLock<Cell<Option<u32>>>>) {
    let mut i = 0;
    loop {
        lock.write().await.with_mut(|v| {
            if v.get_mut().is_none() {
                v.set(Some(i));
                i += 1;
            }
        });
        if i >= LIMIT {
            break;
        }
        yield_now().await;
    }
}

async fn consumer(lock: Rc<RwLock<Cell<Option<u32>>>>) {
    let mut i = 0;
    loop {
        lock.read().await.with(|v| {
            if let Some(v) = v.take() {
                assert_eq!(v, i);
                i += 1;
            }
        });
        if i >= LIMIT {
            break;
        }
        yield_now().await;
    }
}
