use nuuksio::{future::yield_now, runtime::Runtime, sync::Mutex};

use std::{io, rc::Rc};

const LIMIT: u32 = 10;

#[test]
fn main() -> io::Result<()> {
    let rt = Runtime::new()?;

    let mutex = Mutex::new(None);
    let mutex = Rc::new(mutex);

    rt.spawn(consumer(mutex.clone()));
    rt.block_on(producer(mutex.clone()));
    Ok(())
}

async fn producer(mutex: Rc<Mutex<Option<u32>>>) {
    let mut i = 0;
    loop {
        mutex.lock().await.with_mut(|v| {
            if v.is_none() {
                *v = Some(i);
                i += 1;
            }
        });
        if i >= LIMIT {
            break;
        }
        yield_now().await;
    }
}

async fn consumer(mutex: Rc<Mutex<Option<u32>>>) {
    let mut i = 0;
    loop {
        mutex.lock().await.with_mut(|v| {
            if let Some(v) = v.take() {
                assert_eq!(v, i);
                i += 1;
            }
        });
        if i >= LIMIT {
            break;
        }
        yield_now().await;
    }
}
