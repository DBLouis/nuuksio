use nuuksio::{runtime::Runtime, time::sleep};

use tracing::debug;
use tracing_subscriber::EnvFilter;

use std::{env, io, time::Duration};

fn main() -> io::Result<()> {
    let rt = Runtime::new()?;

    let format = tracing_subscriber::fmt::format().with_level(true).with_target(false);
    let filter = EnvFilter::from_default_env();
    tracing_subscriber::fmt().event_format(format).with_env_filter(filter).init();

    let mut args = env::args();
    args.next();

    let duration = Duration::from_secs(args.next().unwrap().parse().unwrap());

    rt.block_on(async move {
        debug!("clock start");
        loop {
            sleep(duration).await.unwrap();
            debug!("tick");
        }
    });

    Ok(())
}
