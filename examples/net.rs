use nuuksio::{
    net::{IpAddr, SocketAddr, TcpListener, TcpStream},
    runtime::Runtime,
    time::sleep,
};

use tracing::{debug, debug_span, info, warn, Instrument};
use tracing_subscriber::EnvFilter;

use std::{io, time::Duration};

// TODO make a more complete example because something is wrong in the reactor
fn main() -> io::Result<()> {
    let rt = Runtime::new()?;
    let saddr = SocketAddr::new(IpAddr::new(127, 0, 0, 1), 8080);

    let format = tracing_subscriber::fmt::format().with_level(true).with_target(false);
    let filter = EnvFilter::from_default_env();
    tracing_subscriber::fmt().event_format(format).with_env_filter(filter).init();

    let handle = rt.spawn(async move {
        let span = debug_span!("client");
        async {
            const MESSAGE: &[u8] = b"Hello world! What a beautiful day!";

            sleep(Duration::from_secs(1)).await.unwrap();

            let mut stream = TcpStream::connect(&saddr).await.unwrap();
            for i in 0..10 {
                let mut buf = [0; 1024];

                info!("sending message {}", i);
                let n = stream.send(MESSAGE).await.unwrap();
                debug!("sent {} bytes", n);

                info!("waiting for response {}", i);
                let n = stream.recv(&mut buf).await.unwrap();
                debug!("received {} bytes", n);
                assert_eq!(&buf[..n], MESSAGE);
            }

            info!("client exiting");
        }
        .instrument(span)
        .await
    });

    rt.block_on(async move {
        let span = debug_span!("server");
        async {
            let mut listener = TcpListener::bind(&saddr).await.unwrap();
            debug!("listening on {}", saddr);

            let (mut stream, saddr) = listener.accept().await.unwrap();
            debug!("accepted connection from {}", saddr);

            let mut buf = [0; 1024];
            for i in 0..10 {
                info!("waiting for message {}", i);
                match stream.recv(&mut buf).await {
                    Ok(n) => {
                        if n == 0 {
                            warn!("connection closed");
                            break;
                        }
                        debug!("received {} bytes", n);
                        info!("sending response {}", i);
                        let n = stream.send(&buf[..n]).await.unwrap();
                        debug!("sent {} bytes", n);
                    }
                    Err(e) => {
                        debug!("failed to read from socket; err = {:?}", e);
                        break;
                    }
                }
            }

            info!("waiting for client");
            handle.join().await;
            info!("client finished");
        }
        .instrument(span)
        .await
    });

    Ok(())
}
