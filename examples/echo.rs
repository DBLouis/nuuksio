use nuuksio::{
    net::{SocketAddr, TcpListener, TcpStream},
    runtime::Runtime,
};

use clap::Parser;
use tracing_subscriber::EnvFilter;

use std::{io, io::prelude::*};

/// A simple echo server and client.
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
enum Args {
    /// Run the echo server.
    Server {
        /// The address to listen on.
        addr: SocketAddr,
    },
    /// Run the echo client.
    Client {
        /// The address to connect to.
        addr: SocketAddr,
    },
}

fn main() -> io::Result<()> {
    let args = Args::parse();

    let format = tracing_subscriber::fmt::format().with_level(true).with_target(false);
    let filter = EnvFilter::from_default_env();
    tracing_subscriber::fmt().event_format(format).with_env_filter(filter).init();

    let rt = Runtime::new()?;
    match args {
        Args::Server { addr } => rt.block_on(server(addr))?,
        Args::Client { addr } => rt.block_on(client(addr))?,
    }

    Ok(())
}

async fn server(addr: SocketAddr) -> io::Result<()> {
    let rt = Runtime::get();

    let mut listener = TcpListener::bind(&addr).await.unwrap();

    loop {
        let (stream, _) = listener.accept().await.unwrap();
        rt.spawn(worker(stream));
    }
}

async fn worker(mut stream: TcpStream) -> io::Result<()> {
    let mut buf = vec![0; 128];

    loop {
        let n = stream.recv(&mut buf).await?;
        if n == 0 {
            return Ok(());
        }
        stream.send(&buf[..n]).await?;
    }
}

async fn client(addr: SocketAddr) -> io::Result<()> {
    let mut stream = TcpStream::connect(&addr).await?;

    let mut buf = vec![0; 128];

    loop {
        let n = io::stdin().read(&mut buf)?;
        if n == 0 {
            stream.shutdown()?;
            return Ok(());
        }
        stream.send(&buf[..n]).await?;
        let n = stream.recv(&mut buf).await?;
        io::stdout().write_all(&buf[..n])?;
    }
}
